#!/bin/sh

if [ -e "$HOME/.zshrc" ]; then
  read -p "An existing '.zshrc' was found. Overwrite? [y/N] " yn
  if [ "$yn" != "y" ]; then
    echo "Not overwriting."
    exit
  fi
fi
cp .zshrc "$HOME/.zshrc" && echo "Installed."
