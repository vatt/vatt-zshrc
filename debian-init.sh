#!/bin/sh

# This is my Debian init script, to run at fresh install and quickly set up the system to my preferences.
# Copyright 2024 Bart Nijbakker
# See LICENSE file for license (GPL-2.0-only)


# Install the following programs:
# * nala (modern APT frontend, available with Debian 12 Bookworm and up)
# * zsh (my preferred shell)
# * zsh-autosuggestions
# * neovim
# * git
# * curl

sudo apt install -y nala
sudo nala install -y zsh zsh-autosuggestions neovim git curl zoxide lnav rsync htop


# Set zsh as default shell for current user

sudo chsh -s `which zsh` `whoami`


# Install my .zshrc using this repository (based on install.sh)

if [ -e "$HOME/.zshrc" ]; then
  read -p "An existing '.zshrc' was found. Overwrite? [y/N] " yn
  if [ "$yn" != "y" ]; then
    echo "Not overwriting."
    exit
  fi
fi
wget -qO .zshrc https://codeberg.org/vatt/vatt-zshrc/raw/branch/main/.zshrc && echo ".zshrc installed"
