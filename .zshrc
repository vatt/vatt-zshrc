# Set up the prompt

## defaults

#autoload -Uz promptinit
#promptinit
#prompt adam1

setopt histignorealldups sharehistory

# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

## my additions

# coloured prompt
# [cyan username (red when root privileges)]@[magenta hostname] [green working dir] [cyan @: reg / red #: root privileges]
autoload -U colors && colors
PS1="%(!.%{$fg[red]%}.%{$fg[cyan]%})%n%{$reset_color%}@%{$fg[magenta]%}%m %{$fg[green]%}%(5~|%-1~/.../%3~|%4~) %(!.%{$fg[red]%}#.%{$fg[cyan]%}%%) %{$reset_color%}"

# coloured ls
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	alias ls='ls --color=auto'
	#alias dir='dir --color=auto'
	#alias vdir='vdir --color=auto'

	#alias grep='grep --color=auto'
	#alias fgrep='fgrep --color=auto'
	#alias egrep='egrep --color=auto'
fi

# kill all background jobs (see https://stackoverflow.com/a/13167238)
alias killbg='kill ${${(v)jobstates##*:*:}%=*}'


# autosuggestions
if [ -e "/usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh" ]; then
	source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
elif [ -e "/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh" ]; then
	source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
else
	echo "'zsh-autosuggestions' is not available. Install it and reopen zsh in order to use autosuggestions."
fi

# Vim mode
#bindkey -v

# Local bins in PATH (ascending order of preference)
# Cargo (Rust programs)
if [ -d "$HOME/.cargo/bin" ]; then
	export PATH="$HOME/.cargo/bin:$PATH"
fi
# PHP Composer bin
if [ -d "$HOME/.config/composer/vendor/bin" ]; then
	export PATH="$HOME/.config/composer/vendor/bin:$PATH"
fi
# ~/.local/bin
if [ -d "$HOME/.local/bin" ]; then
	export PATH="$HOME/.local/bin:$PATH"
fi
# ~/bin
if [ -d "$HOME/bin" ]; then
	export PATH="$HOME/bin:$PATH"
fi

# Aliases
# zoxide (Rust cd upgrade)
if command -v zoxide &>/dev/null; then
	# source its configuration if zoxide is installed
	# this will make `z` a shell function
	. <(zoxide init zsh --cmd z)
fi

# what is a word?
WORDCHARS="${WORDCHARS//\/}" # remove / from word definition

# key bindings
bindkey ' ' magic-space                           # do history expansion on space
bindkey '^R' history-incremental-search-backward  # ctlr + R search history
bindkey '^U' backward-kill-line                   # ctrl + U
bindkey '^[[3;5~' kill-word                       # ctrl + Supr
bindkey '^[[3~' delete-char                       # delete
bindkey '^H' backward-kill-word                   # ctrl + backspace
bindkey '^[^?' backward-kill-word                 # alt + backspace
bindkey '^[[1;5C' forward-word                    # ctrl + ->
bindkey '^[[1;5D' backward-word                   # ctrl + <-
bindkey '^[[1;3C' forward-word                    # ctrl + ->
bindkey '^[[1;3D' backward-word                   # ctrl + <-
bindkey '^[[5~' beginning-of-buffer-or-history    # page up
bindkey '^[[6~' end-of-buffer-or-history          # page down
bindkey '^[[H' beginning-of-line                  # home
bindkey '^[[F' end-of-line                        # end
bindkey '^Z' undo                                 # ctrl + z undo last action
