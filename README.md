# My ZSH Runtime Config

I figured this may be useful for folks.

This makes your zsh relatively easy to use with intuitive shortcuts, while keeping it minimal. It is based on Debian's default zsh config.

## Screenshot

![Screenshot of a terminal runnig zsh](./screenshot.png)

## Dependencies

- ZSH shell installed
- 'zsh-autosuggestions' (optional): to use realtime inline suggestions from your history

## Install

I've written a tiny script to install this config for you, which kindly asks before overwriting any existing files.

```shell
./install.sh
```

## One-liner

This repository also contains a small setup script for Debian systems. It will install the following:

* `nala` (modern APT frontend, available with Debian 12 Bookworm and up)
* `zsh` (my preferred shell)
* `zsh-autosuggestions`
* `neovim`
* `git`
* `curl`
* `zoxide` (a Rust `cd` upgrade, to be used as `z` command, see image below)
* `lnav` (a handy log reader as upgrade to `less`)
* `rsync` (to powerfully copy files around)
* `htop` (a `top` upgrade, with CPU usage bars and colors)
* The `.zshrc` from this repository

To run it, use the following one-liner:

```shell
sh <(wget -qO- https://codeberg.org/vatt/vatt-zshrc/raw/branch/main/debian-init.sh)
```

Please note that piping (`wget -qO- https://... | sh`) does not work, because the script may need STDIN.

An example usage of included `z` command:

![Screenshot of basic z command usage](./screenshot-zoxide.png)

## License

This repository is licensed under GPL-2.0-only, see the [LICENSE file](./LICENSE).
